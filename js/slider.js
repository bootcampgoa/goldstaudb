jQuery(function ($) {
  var roles = ['ES WAR EINMAL …','DIE GESCHICHTE','ZWEIER LIEBENDEN.','BEIDE BEGABEN SICH','AUF EINE REISE.'];
 
  var counter = 0;
  var $role = $('.scroll').fadeOut();
  //repeat the passed function at the specified interval - it is in milliseconds
  setInterval(function () {
      //display the role and increment the counter to point to next role
      $role.text(roles[counter++]).fadeIn();
      //if it is the last role in the array point back to the first item
      if (counter >= roles.length) {
          counter = 0;
      }
  }, 3500);
  
});

jQuery(window).scroll(function(){
  if(jQuery(window).scrollTop() > 180)
  {
      jQuery('.navstyle').addClass('display');
  }
  else if (jQuery(window).scrollTop() < 180)
  {
      jQuery('.navstyle').removeClass('display');
      console.log(jQuery(window).scrollTop());
  }
  });